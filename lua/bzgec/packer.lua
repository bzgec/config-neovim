-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- https://github.com/nvim-telescope/telescope.nvim
    -- Find, Filter, Preview, Pick. All lua, all the time.
    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.x',
        -- branch = '0.1.2',
        -- branch = 'master',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    -- https://github.com/nvim-telescope/telescope-ui-select.nvim
    -- It sets `vim.ui.select` to telescope. That means for example that neovim core stuff can
    -- fill the telescope picker.
    use { 'nvim-telescope/telescope-ui-select.nvim' }
    -- https://github.com/nvim-telescope/telescope-file-browser.nvim
    -- File Browser extension for telescope.nvim
    use {
        "nvim-telescope/telescope-file-browser.nvim",
        requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
    }

    -- Color schemes
    use { "ellisonleao/gruvbox.nvim" }         -- https://github.com/ellisonleao/gruvbox.nvim
    use { "altercation/vim-colors-solarized" } -- https://github.com/altercation/vim-colors-solarized

    -- https://github.com/nvim-treesitter/nvim-treesitter
    -- Parser generator tool and an incremental parsing library.
    -- It can build a concrete syntax tree for a source file and
    -- efficiently update the syntax tree as the source file is edited.
    -- This plugin is only guaranteed to work with specific versions of language parsers
    -- (as specified in the lockfile.json).
    -- When upgrading the plugin, you must make sure that all installed parsers are updated
    -- to the latest version via `:TSUpdate` (note `run = ':TSUpdate').
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }
    -- https://github.com/nvim-treesitter/nvim-treesitter-context
    -- Show code context (function name...)
    use {
        'nvim-treesitter/nvim-treesitter-context',
        --run = ':TSContextEnable'
    }

    -- https://github.com/mbbill/undotree
    -- The undo history visualizer for VIM
    use { "mbbill/undotree" }

    -- https://github.com/tpope/vim-fugitive
    -- Git wrapper
    use { "tpope/vim-fugitive" }

    -- https://github.com/VonHeikemen/lsp-zero.nvim
    -- A starting point to setup some lsp related features in neovim.
    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        requires = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },             -- Required
            { 'williamboman/mason.nvim' },           -- Optional
            { 'williamboman/mason-lspconfig.nvim' }, -- Optional

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },         -- Required
            { 'hrsh7th/cmp-nvim-lsp' },     -- Required
            { 'L3MON4D3/LuaSnip' },         -- Required
            { 'saadparwaiz1/cmp_luasnip' }, -- Optional for autocompletion
            { 'hrsh7th/cmp-nvim-lsp' },     -- Optional for autocompletion
            { 'hrsh7th/cmp-path' },         -- Optional for autocompletion
        }
    }

    -- https://github.com/tpope/vim-commentary
    -- Comment stuff out.
    -- Use `gcc` to comment out a line (takes a count), `gc` to comment out the target of a motion
    -- (for example, `gcap` to comment out a paragraph)
    use { "tpope/vim-commentary" }

    -- https://github.com/ThePrimeagen/harpoon
    -- Mark and navigate files with shortcuts
    use {
        'ThePrimeagen/harpoon',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }

    -- https://github.com/stevearc/oil.nvim
    -- Neovim file explorer: edit your filesystem like a buffer
    use({
        "stevearc/oil.nvim",
    })

    -- https://github.com/natecraddock/workspaces.nvim
    -- Manage workspace directories
    use { 'natecraddock/workspaces.nvim' }

    -- https://github.com/natecraddock/sessions.nvim
    -- Session manager plugin
    use { 'natecraddock/sessions.nvim' }

    -- https://github.com/lambdalisue/suda.vim
    -- Read or write files with sudo command
    use { 'lambdalisue/suda.vim' }

    -- https://github.com/kshenoy/vim-signature
    -- Toggle, display and navigate marks
    use {
        'kshenoy/vim-signature',
    }

    -- https://github.com/iamcco/markdown-preview.nvim
    -- Automatic install (make sure that you have node.js installed)
    use({
        "iamcco/markdown-preview.nvim",
        run = "cd app && npm install",
        setup = function() vim.g.mkdp_filetypes = { "markdown" } end,
        ft = { "markdown" },
    })

    -- https://github.com/alec-gibson/nvim-tetris
    -- Tetris
    use {
        'alec-gibson/nvim-tetris',
    }

    -- https://github.com/sourcegraph/sg.nvim
    -- Cody - SourceGraph
    use { 'sourcegraph/sg.nvim', run = 'nvim -l build/init.lua' }
    -- You'll also need plenary.nvim
end)
