local function get_word_under_cursor()
    return vim.fn.expand("<cword>")
end

local function get_selected_text()
    local a_orig = vim.fn.getreg('a')
    vim.cmd([[silent! normal! "aygv]])
    local text = vim.fn.getreg('a')

    vim.fn.setreg('a', a_orig)

    return text
end

local function get_text()
    local mode = vim.fn.mode()
    if mode == 'v' or mode == 'V' then
        return get_selected_text()
    end
    return get_word_under_cursor()
end

local function close_other_buffers()
    -- Get the current buffer number
    local current_buffer = vim.fn.bufnr('')

    -- Save the cursor position in the current buffer
    local cursor = vim.fn.getpos('.')

    local current_win_view = vim.fn.winsaveview()

    -- Iterate through all buffers
    for _, buf in ipairs(vim.api.nvim_list_bufs()) do
        if buf ~= current_buffer then
            -- Switch to the other buffer
            vim.cmd('buffer ' .. buf)

            -- Close the other buffer
            vim.cmd('bdelete!')

            -- Restore the cursor position in the current buffer
            vim.fn.setpos('.', cursor)
        end
    end
    -- Restore the window view
    vim.fn.winrestview(current_win_view)
end


local function is_buffer(buffer_name)
    -- Iterate over each buffer
    for _, buf in ipairs(vim.api.nvim_list_bufs()) do
        if vim.api.nvim_buf_is_loaded(buf) and vim.api.nvim_buf_is_valid(buf) then
            if vim.api.nvim_buf_get_name(buf) == buffer_name then
                -- The buffer with the target name is found
                return buf
            end
        end
    end

    return nil
end

local function toggle_buffer_split_right(file_path)
    local bufnr = is_buffer(file_path)
    if bufnr ~= nil then
        vim.cmd(":w")
        vim.cmd(":bdelete " .. bufnr)
    else
        vim.cmd(":botright vs " .. file_path)
        vim.cmd(":setlocal bufhidden=hide")
        vim.cmd(":setlocal nobuflisted")
    end
end

local function log_to_file(file_name, text)
    -- Open or create a file in append mode
    local file = io.open(file_name, "a")
    if file then
        -- Write the message to the file
        file:write(text .. "\n")
        -- Close the file
        file:close()
    else
        print("Failed to open file for writing! - " .. file)
    end
end

-- Function to check if cursor is on the last character of the line
local function is_cursor_on_last_character()
    -- Get current cursor position
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    local row, col = cursor_pos[1], cursor_pos[2]

    -- Get the current line content
    local current_line = vim.api.nvim_get_current_line()

    -- Check if cursor is on the last character
    if col == #current_line - 1 then
        return true
    else
        return false
    end
end

local function is_cursor_on_line_start()
    -- Get current cursor position
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    local row, col = cursor_pos[1], cursor_pos[2]
    print(col)

    -- Check if cursor is on the first character of the line
    if col == 0 then
        return true
    else
        return false
    end
end

local function is_end_of_word()
    -- Get the current cursor position
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    local row, col = cursor_pos[1] - 1, cursor_pos[2] -- row is 0-indexed, col is 1-indexed

    -- Get the current line
    local line = vim.api.nvim_get_current_line()

    -- Find the word under the cursor using regex
    local word_start, word_end = line:find("%w+", col + 1)

    -- Adjust word_end to be 0-indexed (because vim's API is 0-indexed)
    word_end = word_end and word_end - 1

    -- Check if the cursor is at the end of the word
    if word_end and col == word_end then
        return true
    else
        return false
    end
end

local function add_text_before_after(before, after)
    local old_yank_text = vim.fn.getreg('"')

    local mode = vim.fn.mode()

    local selected_text = get_text()
    selected_text = before .. selected_text .. after

    -- https://neovim.io/doc/user/builtin.html#feedkeys()
    if mode == 'n' then
        -- Delete current word
        -- vim.fn.feedkeys('diw', 'x')
        vim.fn.feedkeys([["_diw]], 'x')
    else
        -- Delete selected
        vim.fn.feedkeys([["_x]], 'x')
    end

    -- Put text at current cursor position
    if is_cursor_on_last_character() then
        vim.fn.setreg('"', selected_text)
        vim.fn.feedkeys('p', 'x')
    else
        vim.api.nvim_put({ selected_text }, "c", false, false)
    end

    vim.fn.setreg('"', old_yank_text)
end

local function paste_over()
    local mode = vim.fn.mode()

    -- https://neovim.io/doc/user/builtin.html#feedkeys()
    if mode == 'n' then
        -- Cursor must be at the end of the word when in normal mode
        if is_end_of_word() == false then
            vim.fn.feedkeys('e', 'x')
        end
    end

    if is_cursor_on_last_character() then
        if mode == 'n' then
            vim.fn.feedkeys([["_diwp]], 'x')
        else
            vim.fn.feedkeys([["_xp]], 'x')
        end
    else
        if mode == 'n' then
            vim.fn.feedkeys([["_diw]], 'x')
        else
            vim.fn.feedkeys([["_x]], 'x')
        end

        -- Put text at current cursor cursor
        if is_cursor_on_line_start() then
            local text_to_paste = vim.fn.getreg('"')
            vim.api.nvim_put({ text_to_paste }, "c", false, false)
        else
            vim.fn.feedkeys('hp', 'x')
        end
    end
end

return {
    get_word_under_cursor = get_word_under_cursor,
    get_selected_text = get_selected_text,
    get_text = get_text,
    close_other_buffers = close_other_buffers,
    is_buffer = is_buffer,
    toggle_buffer_split_right = toggle_buffer_split_right,
    log_to_file = log_to_file,
    add_text_before_after = add_text_before_after,
    paste_over = paste_over,
}
