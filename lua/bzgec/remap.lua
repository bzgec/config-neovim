local utils = require('bzgec.utils')

-- Set leader key
vim.g.mapleader = " "

-- Search
vim.keymap.set("n", "<leader>sc", "/\\c") -- Case insensitive buffer search

-- Stop search highlighting
vim.keymap.set("n", "<leader>sh", vim.cmd.noh)
vim.keymap.set("n", "<Esc>", vim.cmd.noh)

-- Move selected lines up/down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Keep cursor in the middle when searching
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Keep cursor in center when going page down
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- When selecting text, and pasting over, it remembers previous text (not storing deleted text)
vim.keymap.set({ "n", "v" }, "<leader>p", utils.paste_over)

-- Start search and replace command on selected text (whole word \< \>
vim.keymap.set("x", "R", 'y:%s/\\<<C-r>"\\>')

-- Delete, but don't remember
vim.keymap.set({ "n", "v" }, "<leader>d", "\"_d")

-- Yank to system buffer
vim.keymap.set({ "n", "v" }, "<leader>sy", [["+y]])
vim.keymap.set("n", "<leader>sY", [["+Y]])

-- Paste from system buffer
vim.keymap.set({ "n", "v" }, "<leader>sp", [["+p]])

-- Buffer stuff
vim.keymap.set("n", "<leader>`", ":e#<CR>")                  -- Jump to preciously opened file stored in buffer
vim.keymap.set("n", "<leader>bk", vim.cmd.bd)                -- Unload current buffer
vim.keymap.set("n", "<leader>q", vim.cmd.bd)                 -- Unload current buffer
vim.keymap.set("n", "<leader>bs", vim.cmd.w)                 -- Save buffer
vim.keymap.set("n", "<leader>bo", utils.close_other_buffers) -- Close all other buffers

--vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
--vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
--vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
--vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")

-- Make current file executable, if it has a shebang line
vim.keymap.set("n", "<leader>mfx", "<cmd>!chmod +x %<CR>", { silent = true })

-- Toggle hidden characters
vim.keymap.set("n", "<leader>th", ":set list!<CR>")


-- Toggle scratch buffer
local function ScratchBufferToggle()
    local data_path = vim.fn.stdpath("data")
    local file_path = string.format("%s/scratch-buffer.md", data_path)

    utils.toggle_buffer_split_right(file_path)
end
vim.keymap.set("n", "<leader>x", ScratchBufferToggle)

-- Toggle TODO list file
local function TodoListToggle()
    local data_path = vim.fn.stdpath("data")
    local file_path = string.format("%s/todo.md", data_path)

    utils.toggle_buffer_split_right(file_path)
end
vim.keymap.set("n", "<leader>tl", TodoListToggle)

function SwitchSourceHeaderFile()
    local current_file = vim.fn.expand('%:t') -- Get the current file name
    local file_path = vim.fn.expand('%:p:h')  -- Get the current file's directory

    -- Find the dot in the file name
    local dot_pos = current_file:find('%.')

    -- If a dot is found and the next character is 'c'
    if dot_pos then
        if current_file:sub(dot_pos + 1, dot_pos + 1) == 'c' then
            -- Replace 'c' with 'h'
            -- local new_file = current_file:sub(1, dot_pos) .. 'h'
            local new_file = string.gsub(current_file, "%.c", ".h")

            -- Construct the full path of the new file
            local new_file_path = file_path .. '/' .. new_file

            -- Open the new file
            vim.cmd('edit ' .. new_file_path)
            -- print("edit " .. new_file_path)
        elseif current_file:sub(dot_pos + 1, dot_pos + 1) == 'h' then
            -- Replace 'h' with 'c'
            -- local new_file = current_file:sub(1, dot_pos) .. 'c'
            local new_file = string.gsub(current_file, "%.h", ".c")

            -- Construct the full path of the new file
            local new_file_path = file_path .. '/' .. new_file

            -- Open the new file
            vim.cmd('edit ' .. new_file_path)
            -- print("edit " .. new_file_path)
        end
    else
        print("'.' (dot) not found in file name")
    end
end

vim.keymap.set("n", "<leader>~", SwitchSourceHeaderFile)

local function QuickfixSearchBuffer()
    local selected_text = utils.get_text();

    -- Exit select mode
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Esc>", true, false, true), 'n', true)

    -- Execute buffer quickfix search
    vim.cmd(':%vimgrep /' .. selected_text .. "/ %")
end

-- Quickfix
vim.keymap.set({ "n", "v" }, "<leader>csb", QuickfixSearchBuffer)
vim.keymap.set("n", "<leader>co", ":copen<CR>", {})
vim.keymap.set("n", "<leader>cc", ":cclose<CR>", {})
vim.keymap.set("n", "<leader>cm", ":make ", {})
vim.keymap.set("n", "<C-n>", ':cnext<CR>zz', {})
vim.keymap.set("n", "<C-p>", ':cprevious<CR>zz', {})

-- Cody
vim.keymap.set("n", "<leader>ct", ':CodyToggle<CR>', {})
vim.keymap.set("n", "<leader>ch", ':CodyChat<CR>', {})
vim.keymap.set("v", "<leader>ca", ':CodyAsk<CR>', {})
vim.keymap.set("v", "<leader>ce", ':CodyExplain<CR>', {})

-- Increment/decrement number
vim.keymap.set("n", "g=", '<C-a>', {}) -- Doom Emacs
vim.keymap.set("n", "g-", '<C-x>', {}) -- Doom Emacs

-- Exit terminal mode
vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', {})

-- Window shortcut
vim.api.nvim_set_keymap('n', '<leader>wo', '<C-w>o', {}) -- Close all other windows
vim.api.nvim_set_keymap('n', '<leader>wT', '<C-w>T', {}) -- Move current window to a new tab page
vim.api.nvim_set_keymap('n', '<leader>wn', '<C-w>n', {}) -- New window
vim.api.nvim_set_keymap('n', '<leader>wq', '<C-w>q', {}) -- Close window
vim.api.nvim_set_keymap('n', '<leader>ww', '<C-w>w', {}) -- Move to the next window
vim.api.nvim_set_keymap('n', '<leader>ws', '<C-w>s', {}) -- Horizontal split
vim.api.nvim_set_keymap('n', '<leader>wv', '<C-w>v', {}) -- Vertical split
vim.api.nvim_set_keymap('n', '<leader>w=', '<C-w>=', {}) -- Equalize windows
vim.api.nvim_set_keymap('n', '<leader>w+', '<C-w>+', {}) -- Increase height
vim.api.nvim_set_keymap('n', '<leader>w-', '<C-w>-', {}) -- Decrease height
vim.api.nvim_set_keymap('n', '<leader>w>', '<C-w>>', {}) -- Increase width
vim.api.nvim_set_keymap('n', '<leader>w<', '<C-w><', {}) -- Decrease width
vim.api.nvim_set_keymap('n', '<leader>wh', '<C-w>h', {}) -- Move to window left
vim.api.nvim_set_keymap('n', '<leader>wj', '<C-w>j', {}) -- Move to window down
vim.api.nvim_set_keymap('n', '<leader>wk', '<C-w>k', {}) -- Move to window up
vim.api.nvim_set_keymap('n', '<leader>wk', '<C-w>l', {}) -- Move to window right
vim.api.nvim_set_keymap('n', '<leader>wH', '<C-w>H', {}) -- Move window to the left
vim.api.nvim_set_keymap('n', '<leader>wJ', '<C-w>J', {}) -- Move window to the down
vim.api.nvim_set_keymap('n', '<leader>wK', '<C-w>K', {}) -- Move window to the up
vim.api.nvim_set_keymap('n', '<leader>wK', '<C-w>L', {}) -- Move window to the right


vim.api.nvim_set_keymap('n', '<leader>sfm', ':set foldmethod=manual<CR>', {}) -- Set manual fold method
vim.api.nvim_set_keymap('n', '<leader>sfi', ':set foldmethod=indent<CR>', {}) -- Set indent fold method
vim.api.nvim_set_keymap('n', '<leader>sfs', ':set foldmethod=syntax<CR>', {}) -- Set syntax fold method

-- Toggle LSP diagnostic
local function diagnostic_toggle()
    local config = vim.diagnostic.config
    local vt = config().virtual_text
    config {
        virtual_text = not vt,
        underline = not vt,
        signs = not vt,
    }
end
vim.api.nvim_create_user_command("DiagnosticToggle", diagnostic_toggle, { desc = "Toggle diagnostic" })
vim.keymap.set('n', '<leader>td', diagnostic_toggle, {}) -- Toggle LSP diagnostic


local function set_tab_config(setting)
    if setting == "linux" then
        vim.opt.tabstop = 8
        vim.opt.softtabstop = 8
        vim.opt.shiftwidth = 8
        vim.opt.expandtab = false
    elseif setting == "golang" then
        vim.opt.tabstop = 8
        vim.opt.softtabstop = 8
        vim.opt.shiftwidth = 8
        vim.opt.expandtab = false
    else
        -- Default
        vim.opt.tabstop = 4
        vim.opt.softtabstop = 4
        vim.opt.shiftwidth = 4
        vim.opt.expandtab = true
    end
end
local function set_tab_config_default()
    set_tab_config("default")
end
local function set_tab_config_linux()
    set_tab_config("linux")
end
local function set_tab_config_golang()
    set_tab_config("golang")
end

vim.api.nvim_create_user_command("SetTabConfigDefault", set_tab_config_default,
    { desc = "Set default tab configuration" })
vim.keymap.set('n', '<leader>tid', set_tab_config_default, {})

vim.api.nvim_create_user_command("SetTabConfigLinux", set_tab_config_linux, { desc = "Set linux tab configuration" })
vim.keymap.set('n', '<leader>til', set_tab_config_linux, {})

vim.api.nvim_create_user_command("SetTabConfigGolang", set_tab_config_golang, { desc = "Set golang tab configuration" })
vim.keymap.set('n', '<leader>tig', set_tab_config_golang, {})

vim.keymap.set('n', '<leader>tmp', ':MarkdownPreviewToggle<CR>', {}) -- Toggle Markdown preview (browser)

----------------------------------------------------------------------------------------------------
-- Custom markdown shortcuts
----------------------------------------------------------------------------------------------------
-- Create markdown code from current word or selected text
vim.keymap.set({ 'n', 'v' }, '<leader>mc', function() utils.add_text_before_after("`", "`") end, {})

-- Create markdown link from current word or selected text
vim.keymap.set({ 'n', 'v' }, '<leader>ml', function() utils.add_text_before_after("[", "]()") end, {})

-- Create markdown bold from current word or selected text
vim.keymap.set({ 'n', 'v' }, '<leader>mb', function() utils.add_text_before_after("**", "**") end, {})

-- Create markdown italic from current word or selected text
vim.keymap.set({ 'n', 'v' }, '<leader>mi', function() utils.add_text_before_after("*", "*") end, {})

----------------------------------------------------------------------------------------------------
-- Custom .dts shortcuts
----------------------------------------------------------------------------------------------------
vim.api.nvim_create_autocmd(
    { "BufEnter", "BufWinEnter" }, {
        pattern = { "*.dts" },
        callback = function()
            -- This check is probably not needed if pattern is set correctly
            if vim.bo.filetype == 'dts' then
                -- Set a keymaps for *.dts files only
                vim.api.nvim_buf_set_keymap(0, 'n', 'gcc', [[_i/* <Esc>$a */<Esc>]], { noremap = true })
            end
        end
    }
)
