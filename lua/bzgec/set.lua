vim.opt.nu = true
vim.opt.relativenumber = true

-- Set relative line numbers in netrw - https://stackoverflow.com/a/8731175
vim.g.netrw_bufsettings = 'noma nomod nu nobl nowrap ro'

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

-- Wrap line
vim.opt.wrap = true

-- Don't create backups with vim, but use undotree to create long running undos
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Search configurations
vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8 -- At least 5 lines above and below the cursor
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 1000

vim.opt.colorcolumn = "80"

--vim.opt.language = "en_US.utf8"
vim.cmd.language('en_US.utf8')

-- Configure how hidden characters are shown
vim.opt.list = true -- Display them
vim.opt.listchars = {
    space = '·',
    tab = '→ ',
    eol = '¬',
    precedes = '«',
    extends = '»',
    nbsp = '⍽',
}

vim.opt.spell = true

-- Set custom macros (store to register)
vim.fn.setreg('v', '$xr;') -- 'function() {' -> 'function();'

vim.opt.foldmethod = "indent"
vim.opt.foldcolumn = "auto"
vim.opt.foldlevel = 99 -- Open all folds by default

-- Enable mouse mode, can be useful for resizing splits for example
vim.opt.mouse = 'a'

-- Trim trailing white spaces on buffer save
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
    pattern = { "*" },
    callback = function()
        local save_cursor = vim.fn.getpos(".")
        pcall(function() vim.cmd [[%s/\s\+$//e]] end)
        vim.fn.setpos(".", save_cursor)
    end,
})

-- Highlight when yanking (copying) text
vim.api.nvim_create_autocmd('TextYankPost', {
    desc = 'Highlight when yanking (copying) text',
    group = vim.api.nvim_create_augroup('kickstart-highlight-yank', { clear = true }),
    callback = function()
        vim.highlight.on_yank()
    end,
})
