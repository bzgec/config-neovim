-- https://github.com/tpope/vim-fugitive

-- vim.keymap.set("n", "<leader>gg", vim.cmd.Git) -- Open in horizontal split
vim.keymap.set("n", "<leader>gg", ':tab G<CR>') -- Open in new window (not split)

local function Gitlog()
    vim.cmd(
        ":tab Git log --pretty=format:'%C(yellow)%h %Cred%ad %C(cyan)%an%C(auto)%d %C(reset)%s' --date=format:'%Y/%m/%d %H:%M:%S' --all --graph")
end
vim.keymap.set("n", "<leader>gl", Gitlog)                       -- Open in new window (not split)

vim.keymap.set("n", "<leader>gp", ':Git push origin HEAD<CR>') -- Asynch push
vim.keymap.set("n", "<leader>gf", ':Git fetch --all<CR>')      -- Asynch fetch

-- Resolve conflicts
vim.keymap.set("n", "<leader>gn", "<cmd>diffget //2<CR>") -- Select left window as solution
vim.keymap.set("n", "<leader>go", "<cmd>diffget //3<CR>") -- Select right window as solution
