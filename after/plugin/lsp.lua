-- https://github.com/VonHeikemen/lsp-zero.nvim

local lsp = require('lsp-zero')

lsp.on_attach(function(client, bufnr)
    -- see :help lsp-zero-keybindings
    -- to learn the available actions
    lsp.default_keymaps({
        buffer = bufnr,
    })

    -- https://github.com/VonHeikemen/lsp-zero.nvim#keybindings
    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, {})
    vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, {}) -- In C this would go to your header
    vim.keymap.set("n", "go", function() vim.lsp.buf.type_definition() end,
        { desc = "Jumps to the definition of the type of the symbol under the cursor" })

    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, {})

    vim.keymap.set("n", "gl", function() vim.diagnostic.open_float() end,
        { desc = "Show diagnostics in a floating window" })
    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, {})
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, {})

    -- Format code
    vim.keymap.set("n", "<C-f>",
        vim.lsp.buf.format, { desc = "Format current buffer" })

    -- Lists all the references to the symbol under the cursor in the quickfix window
    vim.keymap.set("n", "<leader>cr", vim.lsp.buf.references)

    -- Renames all references to the symbol under the cursor
    vim.keymap.set("n", "<F2>", function() vim.lsp.buf.rename() end, {})

    -- Selects a code action available at the current cursor position
    vim.keymap.set("n", "<F4>", function() vim.lsp.buf.code_action() end, {})
end)



-- https://github.com/williamboman/mason-lspconfig.nvim#available-lsp-servers
-- Rust: rust-analyzer
-- Python: pylint black flake8 docformatter
-- C: clang-format clangd cpptools cmake-language-server cmakelang cmakelint
-- lua: lua-language-server
-- reStructuredText: rstcheck
-- Markdown: marksman
-- Bash: bashls
--lsp.ensure_installed({
--	'rust-analyzer',
--	'pylint', 'black', 'flake8', 'docformatter',
--	'clang-format', 'clangd', 'cpptools', 'cmake-language-server', 'cmakelang', 'cmakelint',
--	'lua-language-server',
--	'rstcheck',
--})
require("mason").setup({})
require("mason-lspconfig").setup {
    ensure_installed = {
        'lua_ls',
        'marksman',
        'pylsp',
        'gopls',
        'clangd',
        'cmake',
        'rust_analyzer',
        'bashls',
    },

    -- Automatic setup for LSP servers
    handlers = {
        lsp.default_setup,
    }
}

local lspconfig = require("lspconfig")

-- (Optional) Configure lua language server for neovim
lspconfig.lua_ls.setup(lsp.nvim_lua_ls())

-- Configure python setup
lspconfig.pylsp.setup({
    settings = {
        pylsp = {
            plugins = {
                -- formatter options
                black = { enabled = true },
                autopep8 = { enabled = false },
                yapf = { enabled = false },
                -- linter options
                pylint = { enabled = true, executable = "pylint" },
                pyflakes = { enabled = false },
                pycodestyle = { enabled = false },
                -- type checker
                pylsp_mypy = { enabled = true },
                -- auto-completion options
                jedi_completion = { fuzzy = true },
                -- import sorting
                pyls_isort = { enabled = true },
            },
        },
    }
})

-- Configure golang setup
lspconfig.gopls.setup({
    settings = {
        gopls = {
            -- https://github.com/golang/tools/blob/master/gopls/doc/settings.md
            completeUnimported = true,
            usePlaceholders = true,
            analyses = {
                unusedparams = true,
            },
            gofumpt = true,
        },
    },
})

lsp.setup()

-- You need to setup `cmp` after lsp-zero
local cmp = require('cmp')
local luasnip = require('luasnip')
local cmp_action = require('lsp-zero').cmp_action()
cmp.setup({
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    completion = { completeopt = 'menu,menuone,noinsert' },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'path' },
        { name = 'buffer' },
        { name = 'cody' },
    },

    mapping = cmp.mapping.preset.insert({
        -- Accept ([y]es) the completion.
        --  This will auto-import if your LSP supports it.
        --  This will expand snippets if the LSP sent a snippet.
        ['<C-y>'] = cmp.mapping.confirm({ select = true }),

        -- Cancel completion
        -- ['<C-e>']

        -- Select the [n]ext item
        ['<C-n>'] = cmp.mapping.select_next_item(),
        -- Select the [p]revious item
        ['<C-p>'] = cmp.mapping.select_prev_item(),

        -- Manually trigger a completion from nvim-cmp.
        --  Generally you don't need this, because nvim-cmp will display
        --  completions whenever it has completion options available.
        ['<C-Space>'] = cmp.mapping.complete {},

        ['<C-c>'] = cmp.mapping.complete {
            config = {
                sources = {
                    { name = "cody" },
                },
            },
        },

        -- Think of <c-l> as moving to the right of your snippet expansion.
        --  So if you have a snippet that's like:
        --  function $name($args)
        --    $body
        --  end
        --
        -- <c-l> will move you to the right of each of the expansion locations.
        -- <c-h> is similar, except moving you backwards.
        ['<C-l>'] = cmp.mapping(function()
            if luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            end
        end, { 'i', 's' }),
        ['<C-h>'] = cmp.mapping(function()
            if luasnip.jumpable(-1) then
                luasnip.jump(-1)
            end
        end, { 'i', 's' }),

        -- Navigate between snippet placeholder
        --['<C-f>'] = cmp_action.luasnip_jump_forward(),
        --['<C-b>'] = cmp_action.luasnip_jump_backward(),
    })
})

luasnip.config.set_config({
    update_events = { "TextChanged", "TextChangedI" }, -- Update on every change
    exit_roots = false,                                -- Whether snippet-roots should exit at reaching at their last node, $0.
})
