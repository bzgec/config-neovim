-- https://github.com/sourcegraph/sg.nvim
-- https://sourcegraph.com/docs/cody/clients/install-neovim
-- If `SourcegraphLogin` doesn't work add this to `.bashrc`:
-- `export SRC_ENDPOINT="https://sourcegraph.com/"`
-- `export SRC_ACCESS_TOKEN="sgp_fd1b4edb60bf82b8_9210ab399e9d5d0527c6a94d5342f0a7c1abc215"`

-- Sourcegraph configuration. All keys are optional
require("sg").setup {
  -- Pass your own custom attach function
  --    If you do not pass your own attach function, then the following maps are provide:
  --        - gd -> goto definition
  --        - gr -> goto references
  --on_attach = your_custom_lsp_attach_function
}
