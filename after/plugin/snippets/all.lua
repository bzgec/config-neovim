require('luasnip.session.snippet_collection').clear_snippets()

-- https://youtu.be/22mrSjknDHI
-- https://youtu.be/Dn800rlPIho
-- https://youtu.be/KtQZRAkgLqo

local luasnip = require("luasnip")

local s = luasnip.snippet
local i = luasnip.insert_node
local f = luasnip.function_node

local l = require("luasnip.extras").lambda
local dl = require("luasnip.extras").dynamic_lambda

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local same_i = function(index)
    return f(function(arg)
        return arg[1]
    end, { index })
end

luasnip.add_snippets("all", {
    s("snippet-test", fmt("Snippets for all {}", { i(1, "files") })),
})


