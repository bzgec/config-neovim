-- https://github.com/ThePrimeagen/harpoon
local mark = require("harpoon.mark")
local ui = require("harpoon.ui")
local term = require("harpoon.term")

vim.keymap.set("n", "<leader>ha", mark.add_file)
vim.keymap.set("n", "<leader>hh", ui.toggle_quick_menu)

vim.keymap.set("n", "sm", function() ui.nav_file(1) end, {remap = false})
vim.keymap.set("n", "sn", function() ui.nav_file(2) end, {remap = false})
vim.keymap.set("n", "se", function() ui.nav_file(3) end, {remap = false})
vim.keymap.set("n", "si", function() ui.nav_file(4) end, {remap = false})
vim.keymap.set("n", "so", function() ui.nav_file(5) end, {remap = false})
vim.keymap.set("n", "sk", function() ui.nav_file(6) end, {remap = false})
vim.keymap.set("n", "sh", function() ui.nav_file(7) end, {remap = false})
vim.keymap.set("n", "s,", function() ui.nav_file(8) end, {remap = false})
vim.keymap.set("n", "s.", function() ui.nav_file(9) end, {remap = false})
vim.keymap.set("n", "s/", function() ui.nav_file(10) end, {remap = false})

vim.keymap.set("n", "<leader>hq", function() term.gotoTerminal(1) end)
vim.keymap.set("n", "<leader>hw", function() term.gotoTerminal(2) end)
vim.keymap.set("n", "<leader>hf", function() term.gotoTerminal(3) end)
vim.keymap.set("n", "<leader>hp", function() term.gotoTerminal(4) end)
