-- https://github.com/mbbill/undotree

-- Show undo tree
vim.keymap.set('n', '<leader>u', vim.cmd.UndotreeToggle)

