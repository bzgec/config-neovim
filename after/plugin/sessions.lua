require("sessions").setup({
    -- Store all session files in the same location
    session_filepath = vim.fn.stdpath("data") .. "/sessions",
    absolute = true,
})
