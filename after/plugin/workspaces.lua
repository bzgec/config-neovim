workspaces = require("workspaces")
local utils = require('bzgec.utils')

workspaces.setup({
    -- Automatically activate workspace when opening neovim in a workspace directory
    auto_open = false,

    -- Store all workspace files in the same location
    path = vim.fn.stdpath("data") .. "/workspaces",

    hooks = {
        -- Hooks run before change directory
        open_pre = {
            -- Stops session autosaving if enabled. The current state will be saved before stopping
            "SessionsStop",

            -- Stop any active LSP servers
            -- "LspStop",

            -- Delete all buffers (does not save changes)
            "silent %bdelete!",
            -- utils.close_other_buffers
        },

        -- Hooks run after change directory
        open = {
            -- Load any saved session from current directory
            -- (starts autosaving the session on the configured events)
            function(name, path)
                --local sessions_files_path = vim.fn.stdpath("data") .. "/sessions"
                --local session_file_name = path:gsub("/", ".")
                --local session_file_name = session_file_name:gsub("\\", ".")

                ---- Remove leading dot '.'
                --local session_file_name = session_file_name:match("^%.(.*)$") or session_file_name

                ---- Remove trailing dot '.'
                ---- local session_file_name = session_file_name:match("^(.-)%.$") or session_file_name

                --local session_file_name = session_file_name .. "session"

                --local session_file_path = path .. session_file_name
                -- vim.api.nvim_echo({ { session_file_name } }, false, {})
                --vim.api.nvim_echo({ { session_file_path } }, false, {})
                -- if not require("sessions").load(session_file_path, { silent = true }) then
                if not require("sessions").load(nil, { silent = true }) then
                    require("telescope.builtin").find_files({ initial_mode = "insert" })
                    vim.schedule(function() vim.cmd("startinsert") end)
                end
            end
        }
    }
})

vim.api.nvim_set_keymap('n', '<leader>ss', ':SessionsSave<CR>:WorkspacesAdd ', {})
vim.api.nvim_set_keymap('n', '<leader><Tab><Tab>', ':WorkspacesOpen<CR>', {})
vim.api.nvim_set_keymap('n', '<leader>sl', ':WorkspacesOpen<CR>', {})

--vim.api.nvim_set_keymap('n', '<leader>sl', ':SessionManager load_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader>ss', ':SessionManager save_current_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader><Tab>`', ':SessionManager load_last_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader>sd', ':SessionManager delete_session<CR>', {})

--vim.api.nvim_set_keymap('n', '<leader>sl', ':SessionManager load_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader>ss', ':SessionManager save_current_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader><Tab>`', ':SessionManager load_last_session<CR>', {})
--vim.api.nvim_set_keymap('n', '<leader>sd', ':SessionManager delete_session<CR>', {})
