-- https://github.com/nvim-telescope/telescope.nvim
--
-- https://github.com/nvim-telescope/telescope.nvim/blob/master/doc/telescope.txt

local utils = require('bzgec.utils')

local telescope = require('telescope')
local builtin = require('telescope.builtin')

local fb_actions = require("telescope._extensions.file_browser.actions")

telescope.setup({
    extensions = {
        file_browser = {
            display_stat = { date = true, size = true },
            grouped = true,
            preview = {
                ls_short = true,
            },
            mappings = {
                ["i"] = {
                    ["<C-i>"] = fb_actions.toggle_respect_gitignore,
                }
            }
        }
    }
})

-- To get ui-select loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
telescope.load_extension("ui-select")
telescope.load_extension("workspaces")
telescope.load_extension("file_browser")

-- Find all files
local function findAllFiles()
    --builtin.find_files({ hidden = true, no_ignore = true })
    builtin.find_files({ no_ignore = true })
end
--vim.keymap.set('n', '<leader>pf', findAllFiles) -- ThePrimeagen
vim.keymap.set('n', '<leader>.', findAllFiles) -- Doom Emacs

-- Find only git files
--vim.keymap.set('n', '<leader>pg', builtin.git_files, {})       -- ThePrimeagen
vim.keymap.set('n', '<leader><leader>', builtin.git_files, {}) -- Doom Emacs

-- Project search
local function searchProjectFuzzy()
    local mode = vim.fn.mode()
    if mode == 'v' or mode == 'V' then
        local selected_text = utils.get_selected_text()
        builtin.grep_string({ search = '', only_sort_text = true, default_text = selected_text });
    else
        builtin.grep_string({ search = '', only_sort_text = true });
    end
end
local function searchProject()
    local mode = vim.fn.mode()
    if mode == 'v' or mode == 'V' then
        local selected_text = utils.get_selected_text()
        builtin.live_grep({ default_text = selected_text });
    else
        builtin.live_grep({});
    end
end
vim.keymap.set({ 'n', 'v' }, '<leader>/', searchProjectFuzzy)
vim.keymap.set({ 'n', 'v' }, '<leader>?', searchProject)
-- vim.keymap.set('n', '<leader>ps', searchProject)

-- Resume previous search
vim.keymap.set('n', "<leader>'", builtin.resume)

-- Current buffer fuzzy search
local function searchBufferFuzzy()
    local mode = vim.fn.mode()
    if mode == 'v' or mode == 'V' then
        local selected_text = utils.get_selected_text()
        builtin.current_buffer_fuzzy_find({ default_text = selected_text })
    else
        builtin.current_buffer_fuzzy_find()
    end
end
vim.keymap.set({ "n", "v" }, "<leader>b/", searchBufferFuzzy)
vim.keymap.set({ "n", "v" }, "<leader>bs", searchBufferFuzzy)

-- Switch to Buffer
local function showBuffers()
    builtin.buffers({ sort_mru = true, ignore_current_buffer = true })
end
--vim.keymap.set('n', '<leader>pb', showBuffers) -- ThePrimeagen
vim.keymap.set('n', '<leader>,', showBuffers) -- Doom Emacs

-- Find symbols
vim.keymap.set("n", "<leader>si", builtin.treesitter) -- Lists Function names, variables, from Treesitter!
vim.keymap.set("n", "<leader>sd", builtin.lsp_document_symbols)
vim.keymap.set("n", "<leader>sw", builtin.lsp_dynamic_workspace_symbols)

-- Quickfix
-- vim.keymap.set("n", "<leader>co", builtin.quickfix, {}) -- Open quickfix list

-- Git
vim.keymap.set("n", "<leader>gc", builtin.git_commits, {})  -- Show log with diff, <CR> to checkout
vim.keymap.set("n", "<leader>gb", builtin.git_bcommits, {}) -- Show log with diff for current buffer, <CR> to checkout
vim.keymap.set("n", "<leader>gs", builtin.git_status, {})   -- Git status with diff
