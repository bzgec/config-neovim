-- https://github.com/ellisonleao/gruvbox.nvim
-- https://github.com/altercation/vim-colors-solarized

function SetColorScheme(color)
    color = color or "gruvbox"
    vim.cmd.colorscheme(color)
    -- vim.cmd([[colorscheme gruvbox]])
    --vim.o.background = "dark" -- or "light" for light mode

    -- 0 is global space (every windows gets this)
    -- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
    -- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

local function SetColorShemeDark()
    vim.o.background = "dark"
end

local function SetColorShemeLight()
    vim.o.background = "light"
end

-- Set default color scheme and theme
SetColorScheme("gruvbox")
SetColorShemeLight()

-- Set dark/light color scheme
vim.keymap.set("n", "<leader>ttd", SetColorShemeDark, {})
vim.keymap.set("n", "<leader>ttl", SetColorShemeLight, {})

-- Set colorscheme
vim.keymap.set("n", "<leader>ttg", function() SetColorScheme("gruvbox") end, {})
vim.keymap.set("n", "<leader>tts", function() SetColorScheme("solarized") end, {})

