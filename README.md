# My Neovim notes/setup

## Setup
1. Install plugin manager

   1. Download [packer](https://github.com/wbthomason/packer.nvim)

      - Linux: `git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim`
        - If using Flatpak (`~/.var/app/io.neovim.nvim/data/nvim/site` in place of`~/.local/share/nvim/site`):
          `git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.var/app/io.neovim.nvim/data/nvim/site/pack/packer/start/packer.nvim`

      - Windows: `git clone https://github.com/wbthomason/packer.nvim "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"`

   2. Restart Neovim
   3. Install `packer`: `:PackerSync`
   4. Install python lsp packages: `:PylspInstall pyls-flake8 pylsp-mypy python-lsp-black pyls-isort`

2. Move this repo to specified location:

   - On Linux move this configuration to `~/.config/nvim`
     - If using Flatpak move to `~/.var/app/io.neovim.nvim/config/nvim`
   - On windows move this configuration to `~/AppData/Local/nvim`

3. Create workspaces:

   - `cd` in that folder, open a file from that folder then save the session `<leader>ss`
   - Repeat for other folders
   - Load session with `<leader>sl`


## Default navigation
Default navigation for Neovim is `Netrw` (`:h netrw`).

Netrw makes reading files, writing files, browsing over a network, and local browsing easy. It supports many protocols like `scp`, `ftp`, `http`, `rsync`, `sftp`, `fetch`, `dav`, etc (`:h netrw-ref`).

From within Neovim, we can use the `:edit` or `:e` commands to browse the directory. E.g. `:e $HOME` to browse the home directory, or `:e .` to browse the current directory.

- Explore the directory of the current file: `:Ex`
- Horizontal Split & Explore: `:Hex`
- Vertical Split & Explore: `:Wex`
- Tab and Explore: `:Tex`
- Split & Explore the current file's directory: `:Sex`
- Return to/from the Explorer: `:Rex`

### When in Netrw:
- Move to parent folder: `-`
- Create file: `%`
- Create directory: `d`
- Delete: `D`

## Random notes
- Install LSP servers: `:Mason` and press `g?` to show help

  - If LSP servers doesn't start:
    - Ensure the executable is on your PATH: `:lua require('lsp-zero.check').executable('marksman')`
    - Inspect the log file: `:LspLog`
    - Check for LSP information: `:LspInfo`
    - More information: https://github.com/VonHeikemen/lsp-zero.nvim/blob/v3.x/doc/md/guides/what-to-do-when-lsp-doesnt-start.md

- Set working directory:

  - in `Netrw`: `cd`
  - `:cd PATH_TO_DIR`

- Open link under the cursor: `gx`
- Source current lua file: `:so`
- Paste from external programs: `"+p` (register `+`, synced with system clipboard, means from register `+` paste)
- Yank selected from Neovim to system clipboard: `"+y` (register `+`, synced with system clipboard, means yank to register `+`)
- Select code and press `=` to align the code
- Show mappings (shortcuts) and commands defined by vim itself: `:help index`
- Show mappings defined by your vimrc and plugins: `:map`
- Show mappings defined by your vimrc and plugins plus where they are defined: `:verbose map`
- Show mappings defined by your vimrc and plugins (that work in normal mode): `:nmap`
- Show mappings defined by your vimrc and plugins plus where they are defined (that work in normal mode): `:nverbose map`
- Show mappings in telescope: `:Telescope keymaps`
- Join [count] lines (current and line below) to a single line: `J`
- Move cursor line to the middle of the screen: `zz` (`zt` and `zb` to move to top and bottom)
- Move half page down/up: `<C-d>`/`<C-u>`
- Move to the first non space character of the line and stay in normal mode: `_`
- Delete current word: `diw`
- Delete current text between white spaces: `diW`
- Delete current word and enter insert mode: `ciw`
- Copy inside parenthesis: `ci)`
- Delete inside `"`: `di"`
- Copy inside curly braces including the braces: `ca{`
- Delete inside parenthesis and enter insert mode: `ci)`
- Select first autocomplete option: `C-y`
- Close all other windows: `C-w o`
- Set all windows to the same size: `C-w =`
- You can save/write unwritable files: `:SudaWrite`
- Select text between curly braces: `va}`
- Open two files in diff mode (use fold motions to open/close folds): `nvim -d FILE_1 FILE_2`
- Move to the other side of `{}`, `[]` or `()` with `%`
- Repeat an `f` command
  - forward: `;`
  - reverse: `,`
- Show default documentation about a function, variable (includes Doxygen): `K`
- Repeat inserted text multiple times:
  1. `5i`
  2. Type some characters
  3. `<Esc>`
  Characters now repeated.
- For the word under/after the cursor suggest correctly spelled words: `z=`
- Search word under cursor: `*` or backwards `#`
- To search for any character use `.` and to search any number of characters use `.*`
- To search case insensitive use `\c` (can be placed anywhere)
- Search in opposite direction (up): `?` (`N` to reverse direction)
- Move to the beginning of the next word `w`, `e` to the end, `W` and `E` skip punctuation marks
- Move to the beginning of the previous word `b`, `ge` to the end, `B` and `gE` skip punctuation marks
- Yank (copy) whole document: `:%y` (`%` means whole document)
- `A` to enter insert mode at the end of the line (`I` at the beginning)

### Quickfix
- Set program for finding errors: `:set makeprg=PROGRAM`
- Execute program: `<leader>cm`
- Search buffer with quickfix: `<leader>csb`
- Search all files with quickfix: `:vimgrep /SEARCH_STR/ **/.X` where `X` is file ending like `c` or `py`
- Search all files using telescope, then press `<C-q>` to add results to quickfix list
- Open quickfix list: `<leader>co`
- Close quickfix list: `<leader>cc`
- Move between options with: `<C-n>` and `<C-p>`
- Execute command on all quicklist entries: `:cdo CMD` (CMD can be replace command like `s/TEXT/TEKST/gc`)
  - To save files after `cdo` command append ` | update`: `:cdo CMD | update`
- Lists all the references to the symbol under the cursor in the quickfix window: `<leader>cr`

### Resolve git conflicts
1. Open `fugitive`: `<leader>gg`
2. Move to file which has conflicts
3. Open split view of the file under the cursor: `dv`
4. Move to the conflict (middle window)
5. Press `<leader>gn` to select left solution or press `<leader>go` to select right solution
6. Save solutions: `:w`

Note that `dv` can be used on the stashed/unstashed changes, not only for conflicts.

### Marks
- Set mark: `m[a-Z]`
- Go to mark: `'[a-Z]`
- File marks: `[a-z]`
- Global marks: `[A-Z]`
- Delete mark: `dm[a-Z]`
- List all marks: `:marks`
- Jump to position where last change occurred in current buffer: '`.'
- Jump to position where last exited current buffer: '`"'
- Jump back (to line in current buffer where jumped from): `''`
- Jump back (to position in current buffer where jumped from): '`'
- Jump to next mark: `]'`
- Jump to previous mark: `['`

- https://vim.fandom.com/wiki/Using_marks

### Folds

- https://vim.fandom.com/wiki/Folding

- Manual folds: `:set foldmethod=manual` (`<leader>sfm`)

  - Fold selected text: visually select text then press `zf`
  - Fold from current line to mark `a`: `zf'a`
  - Fold current line along with the following 3 lines: `zf3j`
  - Fold current code block (code must use `{` and `}`: `zfa}`

- Indent folds: `:set foldmethod=indent` (`<leader>sfi`)
- Syntax folds: `:set foldmethod=syntax` (`<leader>sfs`)

- Open fold: `zo`
- Close fold: `zc`
- Toggle fold: `za`
- Open all folds: `zR`
- Close all folds: `zM`
- Delete fold (reopen): `zd`
- Delete folds recursively: `zD`
